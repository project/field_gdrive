<?php

/**
 * @file
 * Field module functionality for the Gdrive Field module.
 */

define('FIELD_GDRIVE_UPLOAD_LOCATION', 'public://gdrive_uploads');

/**
 * Implements hook_field_info().
 */
function field_gdrive_field_info() {
  return array(
    'gdrive' => array(
      'label' => t('Gdrive'),
      'description' => t('This field stores the ID of a file as an integer value.'),
      'settings' => array(
        'uri_scheme' => variable_get('file_default_scheme', 'public'),
      ),
      'instance_settings' => array(
        'file_extensions' => 'txt pdf doc docx xls xlsx',
        'max_filesize' => '5 MB',
      ),
      'default_widget' => 'gdrive_widget',
      'default_formatter' => 'gdrive_default',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function field_gdrive_field_widget_info() {
  return array(
    'gdrive_widget' => array(
      'label' => t('Gdrive'),
      'field types' => array(
        'gdrive',
      ),
    ),
  );
}

/**
 * Implements hook_field_instance_settings_form().
 */
function field_gdrive_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];

  $extensions = str_replace(' ', ', ', $settings['file_extensions']);
  $form['file_extensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed file extensions'),
    '#default_value' => $extensions,
    '#description' => t('Separate extensions with a space or comma and do not include the leading dot.'),
    '#element_validate' => array(
      'field_gdrive_settings_extensions_validate',
    ),
    '#weight' => 1,
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['max_filesize'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum upload size'),
    '#default_value' => $settings['max_filesize'],
    '#description' => t('Enter a value like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes) in order to restrict the allowed file size. If left empty the file sizes will be limited only by PHP\'s maximum post and file upload sizes (current limit <strong>%limit</strong>).', array(
      '%limit' => format_size(file_upload_max_size()),
    )),
    '#size' => 10,
    '#element_validate' => array(
      'field_gdrive_settings_max_filesize_validate',
    ),
    '#weight' => 2,
  );
  $form['display_download_url'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display download url'),
    '#default_value' => isset($settings['display_download_url']) ? $settings['display_download_url'] : '',
    '#description' => t('Check this to display download link on node display page.'),
    '#weight' => 3,
  );

  return $form;
}

/**
 * Element validate callback for the allowed file extensions field.
 */
function field_gdrive_settings_extensions_validate($element, &$form_state) {
  if (!empty($element['#value'])) {
    $extensions = preg_replace('/([, ]+\.?)/', ' ', trim(strtolower($element['#value'])));
    $extensions = array_filter(explode(' ', $extensions));
    $extensions = implode(' ', array_unique($extensions));
    if (!preg_match('/^([a-z0-9]+([.][a-z0-9])* ?)+$/', $extensions)) {
      form_error($element, t('The list of allowed extensions is not valid, be sure to exclude leading dots and to separate extensions with a comma or space.'));
    }
    else {
      form_set_value($element, $extensions, $form_state);
    }
  }
}

/**
 * Element validate callback for the maximum upload size field.
 */
function field_gdrive_settings_max_filesize_validate($element, &$form_state) {
  if (!empty($element['#value']) && !is_numeric(parse_size($element['#value']))) {
    form_error($element, t('The "!name" option must contain a valid value. You may either leave the text field empty or enter a string like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes).', array(
      '!name' => $element['title'],
    )));
  }
}

/**
 * Implements hook_field_widget_form().
 */
function field_gdrive_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  switch ($instance['widget']['type']) {
    case 'gdrive_widget':
      $result = NULL;
      if (isset($element['#entity']->nid)) {
        $nid = $element['#entity']->nid;
        $result = field_gdrive_load_drive_file($nid);
      }

      // If gdrive field has file. Display link to google drive file and
      // remove button to delete file from google drive.
      if (isset($result['gid'])) {
        $display_download_url = $instance['settings']['display_download_url'];
        $url = field_gdrive_file_link($result['entity_id'], $display_download_url);

        $element['gdrive_container'] = array(
          '#type' => 'fieldset',
          '#title' => t('URL to google drive file'),
          '#tree' => TRUE,
          '#prefix' => '<div id="gdrive-element-wrapper">',
          '#suffix' => '</div>',
        );
        $element['gdrive_container']['fid'] = array(
          '#markup' => "<div class='form-item'>" . $url . "</div>",
        );
        $element['gdrive_container']['remove_gdrive_file'] = array(
          '#type' => 'submit',
          '#value' => t('Remove file from google drive.'),
          '#submit' => array(
            'field_gdrive_ajax_remove_handler',
          ),
          '#attributes' => array(
            'data' => array(
              $result['entity_id'],
              $result['gid'],
            ),
          ),
          '#name' => 'remove_name_' . $delta,
        );
      }
      else {
        // If field is empty or no google drive file.
        $desc = 'Upload file to save on google drive. Allowed file extensions are ' . $instance['settings']['file_extensions'] . '. Max allowed file size is ' . $instance['settings']['max_filesize'] . '.';
        $element['fid'] = array(
          '#type' => 'managed_file',
          '#weight' => 8,
          '#title' => $element['#title'],
          '#default_value' => isset($element['#value']['fid']) ? $element['#value']['fid'] : 0,
          '#description' => $desc,
          '#required' => $element['#required'],
          '#upload_location' => FIELD_GDRIVE_UPLOAD_LOCATION,
          '#upload_validators' => array(
            'file_validate_extensions' => array(
              $instance['settings']['file_extensions'] ? $instance['settings']['file_extensions'] : 'txt pdf doc docx xls xlsx',
            ),
            'file_validate_size' => array(
              $instance['settings']['max_filesize'] ? ($instance['settings']['max_filesize'] * 1024 * 1024) : 5 * 1024 * 1024,
            ),
          ),
        );
      }
      break;
  }

  return $element;
}

/**
 * Submit callback.
 */
function field_gdrive_ajax_remove_handler($form, &$form_state) {
  // Delete google drive file and update drive file data.
  $entity_id = $form_state['triggering_element']['#attributes']['data'][0];
  $file_id = $form_state['triggering_element']['#attributes']['data'][1];
  field_gdrive_edit_drive_file($entity_id, $file_id);

  $form_state['rebuild'] = TRUE;
  drupal_get_messages();
}

/**
 * Implements hook_field_is_empty().
 */
function field_gdrive_field_is_empty($item, $field) {
  return !isset($item);
}

/**
 * Implements hook_field_formatter_info().
 */
function field_gdrive_field_formatter_info() {
  return array(
    'gdrive_default' => array(
      'label' => t('Gdrive'),
      'field types' => array(
        'gdrive',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function field_gdrive_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($instance['widget']['type']) {
    case 'gdrive_widget':
      $display_download_url = $instance['settings']['display_download_url'];
      $url = field_gdrive_file_link($entity->nid, $display_download_url);
      if (empty($items)) {
        $element[]['#markup'] = $url;
        $element['#items'][] = array(
          'value' => $url,
        );
        $items[] = array(
          'value' => $url,
        );
      }
      break;
  }

  return $element;
}

/**
 * Generate formatted link for google drive file.
 *
 * @param int $nid
 *   Node id of current node.
 * @param int $display_download_url
 *   Setting whether to display download link or not.
 *
 * @return string
 *   Return formatted html output for google drive file with download link.
 */
function field_gdrive_file_link($nid, $display_download_url) {
  drupal_add_css(drupal_get_path('module', 'field_gdrive') . '/css/field_gdrive.css');
  $gdrive_file = field_gdrive_load_drive_file($nid);

  $options = array(
    'attributes' => array(
      'class' => array(
        'node-' . $nid,
      ),
      'id' => $gdrive_file['gid'],
      'target' => '_blank',
    ),
  );

  $url = l($gdrive_file['filetitle'], $gdrive_file['fileurl'], $options);

  if (1 == $display_download_url) {
    $download_url = l('<i class="sprite sprite-drive-download"></i>', $gdrive_file['downloadurl'], array(
      'html' => TRUE,
    ));

    $output = '<div class="gdrive-field-container clearfix">
      <div class="col-70">
        <i class="sprite sprite-' . $gdrive_file["filemime"] . '"></i>' . $url . '
      </div>
      <div class="drive-download col-30">' . $download_url . '</div>
    </div>';
  }
  else {
    $output = '<div class="col-md-100">
        <i class="sprite sprite-' . $gdrive_file["filemime"] . '"></i>' . $url . '
      </div>';
  }

  return $output;
}
