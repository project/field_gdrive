This module is used to upload files to google drive directly. It provides a 
field type Gdrive in the field UI.

This module bypasses the local storage and upload the file directly to a 
configured google drive account and displays the file link on node detail page.

Dependencies:
----------------

1. Droogle Module (https://www.drupal.org/project/droogle).

NOTE: Before installing the Google drive field module enable and configure 
Droogle module for proper functionality.

Installation:
----------------
1. Install and configure Droogle module.
2. Enable Google drive field module.
3. Go to manage fields in any content type and add new field of type Gdrive.

Configuration:
---------------
1. Go to admin/config/services/field-gdrive and select the permission for file.

Limitations:
----------------
Currently it supports only one file upload to google drive with one node.
