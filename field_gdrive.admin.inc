<?php

/**
 * @file
 * Admin page callbacks for the field_gdrive module.
 */

/**
 * The settings form function.
 */
function field_gdrive_settings() {
  $options = array(
    'user' => 'User',
    'group' => 'Group',
    'anyone' => 'Anyone',
  );
  $form['field_gdrive_permission'] = array(
    '#type' => 'radios',
    '#title' => t('Select Permission'),
    '#options' => $options,
    '#default_value' => variable_get('field_gdrive_permission', 'user'),
  );
  $form['field_gdrive_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter domain'),
    '#default_value' => variable_get('field_gdrive_domain', ''),
    '#states' => array(
      'visible' => array(
        array(
          ':input[name="field_gdrive_permission"]' => array(
            'value' => 'domain',
          ),
        ),
      ),
      'required' => array(
        ':input[name="field_gdrive_permission"]' => array(
          'value' => 'domain',
        ),
      ),
    ),
    '#element_validate' => array(
      'field_gdrive_domain_validate',
    ),
  );
  $form['field_gdrive_group_mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter group email'),
    '#default_value' => variable_get('field_gdrive_group_mail', ''),
    '#states' => array(
      'visible' => array(
        array(
          ':input[name="field_gdrive_permission"]' => array(
            'value' => 'group',
          ),
        ),
      ),
      'required' => array(
        ':input[name="field_gdrive_permission"]' => array(
          'value' => 'group',
        ),
      ),
    ),
    '#element_validate' => array(
      'field_gdrive_group_validate',
    ),
  );

  return system_settings_form($form);
}

/**
 * Form Domain validate callback.
 */
function field_gdrive_domain_validate($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['field_gdrive_permission'] == "domain" && empty($values['field_gdrive_domain'])) {
    form_set_error('field_gdrive_domain', t('Domain field is required.'));
  }
}

/**
 * Form group validate callback.
 */
function field_gdrive_group_validate($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['field_gdrive_permission'] == "group" && empty($values['field_gdrive_group_mail'])) {
    form_set_error('field_gdrive_group_mail', t('Group field is required.'));
  }
}
